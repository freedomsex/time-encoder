<?php

namespace A4Sex\Tests;

use A4Sex\TimeEncoder;
use PHPUnit\Framework\TestCase;

class TimeEncoderTest extends TestCase
{
    private TimeEncoder $object;

    public function setUp(): void
    {
        parent::setUp();
        $this->object = new TimeEncoder('12345678900987654321123456789009876543211234567890098765432112345678900987654321', 10);
    }

    public function testParse()
    {
        $token = $this->object->token(5);
        $data = $this->object->parse($token);
        self::assertTrue($data->claims()->has('time'));
        $data = $this->object->parse('some-time-token');
        self::assertNull($data);
    }

    public function testCheckout()
    {
        $token = $this->object->token(5);
        self::assertFalse($this->object->checkout($token));
        $token = $this->object->token(0);
        self::assertTrue($this->object->checkout($token));
    }

    public function testCheckoutSome()
    {
        self::assertFalse($this->object->checkout('some-time-token'));
    }

    public function testCheckoutFor()
    {
        $token = $this->object->token(5, 'some', 'localhost');
        self::assertFalse($this->object->checkout($token));
        self::assertTrue($this->object->checkout($token, 'localhost'));
    }

    public function testToken()
    {
        $token = $this->object->token(5);
        self::assertEquals(3, count(explode('.', $token)));
    }
}
